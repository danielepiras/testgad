<?php

namespace Database\Seeders;

use App\Http\Requests\DataRequest;
use App\Models\Data;
use Faker\Core\Number;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use phpDocumentor\Reflection\PseudoTypes\Numeric_;
use PhpOffice\PhpSpreadsheet\Calculation\MathTrig\Random;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        data::factory()
            ->count(10)
            ->create();
            // DB::table('data')->insert([
            // 'name' => Str::random(10),
            // 'email' => Str::random(10).'@gmail.com',
            // 'surname' =>Str::random(10),
            // 'region' =>Str::random(10),
            // 'province' =>Str::random(10),
            // 'city' =>Str::random(10),
            // 'address' =>Str::random(10),
            // 'cap' =>rand(6,7),
            // ]);
    }
}
