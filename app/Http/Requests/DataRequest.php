<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'surname' => 'required|string',
            'email' => 'required|email',
            'region' => 'required|string',
            'province' => 'required|string',
            'city' => 'required|string',
            'address' => 'required|string',
            'cap' => 'required|digits:5',
            
        ];
    }

    public function messages()
{
    return [
        'name.required' => 'Il nome è obbligatorio',
        'surname.required' => 'Il congome è obbligatorio',
        'email.required' => "L'email è obbligatoria",
        'region.required' => 'La regione è obbligatoria',
        'province.required' => 'La provincia è obbligatoria',
        'city.required' => 'La città è obbligatoria',
        'address.required' => "L'indirizzo è obbligatorio",
        'cap.required' => 'Il cap è obbligatorio',
        'cap.digits' => "Il cap dev'essere di 5 numeri",
        'email.email' => 'Email non valida',
        

    ];
}
}
