<?php

use App\Http\Controllers\DataController;
use App\Http\Controllers\ImportFileController;
use App\Http\Controllers\PersonalDataController;
use App\Models\ImportFile;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Message;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');

Route::group(['middleware' => 'auth'], function () {
    
    Route::get('/data/create', [DataController::class, 'view'])->name('data.create');
    Route::post('/data/store', [DataController::class, 'store']);
    Route::get('/data', [DataController::class, 'index' ])->name('data');
    Route::get('/data/edit/{data}', [DataController::class, 'edit'])->name('data.edit');
    Route::post('/data/update', [DataController::class, 'update'])->name('data.update');
    Route::get('/data/delete/{data}', [DataController::class, 'destroy'])->name('data.delete');
    Route::get('/data/import', [DataController::class, 'import'])->name('data.import');
    Route::post('/store', [DataController::class, 'storeExcel']);
    
});



