<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    use HasFactory;

    protected $fillable = [
    'name',
    'surname',
    'email',
    'region',
    'province',
    'city',
    'address',
    'cap',
    
    ];


}
