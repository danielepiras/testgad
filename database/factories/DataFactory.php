<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Data>
 */
class DataFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->firstName(),
            'surname' => $this->faker->lastName(),
            'address' => $this->faker->streetAddress(),
            'province' => $this->faker->name(),
            'region' => $this->faker->name(),
            'city' => $this->faker->city(),
            'cap' => $this->faker->randomNumber(5,5),
            'email' => $this->faker->unique()->safeEmail(),
            
        ];
    }
}
