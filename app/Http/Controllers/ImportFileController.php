<?php

namespace App\Http\Controllers;

use App\Http\Requests\DataRequest;
use App\Imports\ExcelImport;
use Inertia\Inertia;
use App\Models\ImportFile;
use BaconQrCode\Renderer\Path\Path;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Redirect;

class ImportFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Data/Import');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('file');
        
        Excel::import(new ExcelImport, $file);
       return Redirect::route('data');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ImportFile  $importFile
     * @return \Illuminate\Http\Response
     */
    public function show(ImportFile $importFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ImportFile  $importFile
     * @return \Illuminate\Http\Response
     */
    public function edit(ImportFile $importFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ImportFile  $importFile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ImportFile $importFile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ImportFile  $importFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(ImportFile $importFile)
    {
        //
    }
}
