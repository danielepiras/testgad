<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}



// $data = PersonalData::create([
//     'name'=>$request->name,
//     'surname'=>$request->surname,
//     'email'=>$request->email,
//     'region'=>$request->region,
//     'province'=>$request->province,
//     'city'=>$request->city,
//     'streetAddress'=>$request->streetAddress,
//     'postalCode'=>$request->postalCode,
// ]);