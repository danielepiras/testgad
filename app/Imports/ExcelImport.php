<?php

namespace App\Imports;

use App\Http\Requests\DataRequest;
use App\Models\Data;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ExcelImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        
            
        return new Data([
            'name' => $row['name'],
            'surname' => $row['surname'],
            'email' => $row['email'],
            'region' => $row['region'],
            'province' => $row['province'],
            'city' => $row['city'],
            'address' => $row['address'],
            'cap' => $row['cap'],
            
        

        ]);
    
        
    }
}