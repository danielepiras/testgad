<?php

namespace App\Http\Controllers;

use App\Models\Data;
use Inertia\Inertia;
use App\Imports\DataImport;
use App\Imports\ExcelImport;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\FuncCall;
use App\Http\Requests\DataRequest;
use App\Models\ImportFile;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Redirect;
use Symfony\Contracts\Service\Attribute\Required;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $validations; 

    public function __construct()
    {
        $this->validations = [
            'name' => 'required|string',
            'surname' => 'required|string',
            'email' => 'required|email',
            'region' => 'required|string',
            'province' => 'required|string',
            'city' => 'required|string',
            'address' => 'required|string',
            'cap' => 'required|digits:5',
        ];
    }
    public function index()
    {

    $data = Data::query()
        ->orderBy(column: 'updated_at')
        ->get();
        return Inertia::render('Data/Data', [
            'data' => $data,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DataRequest $request)
    {
        $postData = $this->validate($request, $this->validations);

        data::create([
            'name' =>$postData['name'],
            'surname' =>$postData['surname'],
            'email' =>$postData['email'],
            'region' =>$postData['region'],
            'province' =>$postData['province'],
            'city' =>$postData['city'],
            'address' =>$postData['address'],
            'cap' =>$postData['cap'],
        ]);

        

        return redirect('data');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function show(Data $data)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function edit(Data $data)
    {
        return Inertia::render('Data/DataEdit', [
            'data-prop'=> $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function update(DataRequest $request, Data $data)
    {   
        $rules = $this->validations;

        $rules['id'] = 'required';

        $postData = $this->validate($request, $rules);

        $data = Data::where('id', $postData['id'])->update($postData);

        return redirect()->route('data', ['data'=> $postData['id']]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function destroy(Data $data)
    {
        $data->delete();
        return Redirect::route('data');
    }

    public function view(){
        return Inertia::render('Data/DataCreate');
    }

    public function import(){
        // Creazione permessi e ruoli con spatie
        // $role = Role::create(['name' => 'import']);
        // $permission = Permission::create(['name' => 'import file']);
        // Indentificazione utente loggato
        $user = Auth::user();
        // Aquisizione permessi spatie
        // $user->givePermissionTo('import file');

        if ($user->hasPermissionTo('import file') == true) {
            # code...
            return Inertia::render('Data/Import');
        }
        return Redirect::route('data')->with('error', 'Non possiedi i requisiti per caricare un file');
    } 
        
    public function storeExcel(Request $request)
    {
        $file = $request->file('file');
        
        Excel::import(new ExcelImport, $file);

            return Redirect::route('data');
        
        
    }


    
}
 